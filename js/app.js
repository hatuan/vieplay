var body = document.querySelector('body');
var footer = document.querySelector('footer');
var header = document.getElementById("header");
var htmlLoading ='<div class="loading"><i></i></div>';

var sticky = (header) ? header.offsetTop : 0;
var timeline = document.getElementById("timeline-section-list");
let width15min = 137;
var mq = window.matchMedia( "(max-width: 1440px)" );
if (mq.matches) {
    width15min = 115;
}
var mq = window.matchMedia( "(max-width: 1024px)" );
if (mq.matches) {
    width15min = 72;
}
var mq = window.matchMedia( "(width: 768px)" );
if (mq.matches) {
    width15min = 45;
}
let widthSecond = width15min / 15 / 60;
let widthTimeLine = width15min * 4 * 24;
let maxWidthTimeLine = width15min * 4 * 21;
let timelineContent = document.querySelector('#timeline-section-list');
let scheduleChannelContent = document.querySelectorAll('.schedule-channel');
var seeMoreDes = document.querySelectorAll('#see-more-des');
var contentDes = $('.content-des');
var programs = document.querySelectorAll('.schedule-channel li a');
var addToWishlist = document.querySelectorAll('.add-to-wishlist');
var messagePopup = document.querySelector('.message-popup');
let mainPlayer = $('.mainplayer');
let playerControl = $('.player-control');
var btnWatchLater = document.querySelectorAll('.icon-watchlater');
var menuAction = document.querySelectorAll('.menu-action');
var minItems = 2;
var maxItems = 6;
var itemMargin = 20;
var heightMainItemFix = 44;

if (window.innerWidth <= 1440) {
    heightMainItemFix = 48;
}

if (window.innerWidth <= 1024) {
    heightMainItemFix = 44;
}

function initLoadingDom() {
    //$(body).append(htmlLoading);
}

function removeLoadingDom() {
    $('.loading').remove();
}

function initPositionFooter() {
    let windowHeight = $(window).height();
    let bodyHeight = $(body).height();

    if (bodyHeight < windowHeight) {
        $(footer).addClass('footer-sticky');
    }
}

function stickyHeader() {
    if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
    } else {
        header.classList.remove("sticky");
    }
}

function initOptionMenu() {
    $(document).click(function(e) {
        if (!$(e.target).is('.menu-action-parent, .menu-action, .menu-action img')) {
            if ($(menuAction).parent('.menu-action-parent').hasClass('menu-active')) {
                $(menuAction).parent('.menu-action-parent').removeClass('menu-active');
            }
        }
    });

    $.each(menuAction, function(key,val) {
        $(menuAction[key]).on('click', function(e) {
            e.preventDefault();
            var _this = this;
            $(menuAction).not(_this).parent('.menu-action-parent').removeClass('menu-active');
            $(_this).parent('.menu-action-parent').toggleClass('menu-active');
        });
    });

    
}

function showMoreDes() {
    var heightContentDes = contentDes.height();

    if (heightContentDes > 43) {
        contentDes.find('#see-more-des').show();
        contentDes.css('max-height', 43);
    }

    $.each(seeMoreDes, function(key, value) {
        $(seeMoreDes[key]).on('click', function () {
            var contentDes = $(this).parent();
            $(contentDes).toggleClass('expand');

            if ($(contentDes).hasClass('expand')) {
                $(contentDes).children('#see-more-des').text('thu gọn');
            } else {
                $(contentDes).children('#see-more-des').html('<span>...</span>xem thêm');
            }
        });
    });
}

function showHidePassword() {
    $('.show-password').on('click', function () {
        $(this).toggleClass('fa-eye-slash');
        var inputPassword = $(this).parent().find('input');

        if ($(this).hasClass('fa-eye-slash')) {
            $(inputPassword).attr('type','text');
        } else {
            $(inputPassword).attr('type','password');
        } 
    });
}

function getGridSize() {
    return (window.innerWidth < 1280) ? 6 :
           (window.innerWidth < 1366) ? 6 : 6;
}

function getTotalSpaceItem() {
    var getGridSizeContain = getGridSize();
    return itemMargin * (getGridSizeContain - 1);
}

function createClass(name,rules){
    var style = document.createElement('style');
    style.type = 'text/css';
    document.getElementsByTagName('head')[0].appendChild(style);
    if(!(style.sheet||{}).insertRule) 
        (style.styleSheet || style.sheet).addRule(name, rules);
    else
        style.sheet.insertRule(name+"{"+rules+"}",0);
}

function removeStyle(){
    if (document.getElementsByTagName('style')) {
        var hs = document.getElementsByTagName('style');
        for (var i=0, max = hs.length; i < max; i++) {
            hs[i].parentNode.removeChild(hs[i]);
        }
    }
}

function setSizeMainItem() {
    var ribbonWidth = $('.ribbon-list').width();
    var mainItem = $('.ribbon-list').find('.main-item');
    if (mainItem) {
        let heightMainItem = $(mainItem).next('li').height();
        let totalSpaceItem = getTotalSpaceItem();
        let widthMainItem = ((ribbonWidth - totalSpaceItem) / 6) * 2 + itemMargin;

        createClass('.main-item-double', 'width: ' + widthMainItem + 'px !important');
        $(mainItem).addClass('main-item-double');
        $(mainItem).find(' > a img').css('height', heightMainItem - heightMainItemFix);

    }
}

function addItemLastRibbon() {
    let _htmlMainItemSlide = '<li><ul><li></li><li></li></ul></li>';
    let mainItem = $('.ribbon-list').find('.main-item');
    if (mainItem) {
        $(mainItem).parent('.slides').append(_htmlMainItemSlide);
    }
}

function initFlexSlider() {
    addItemLastRibbon();
    
    var $window = $(window),
      flexslider;

    $('.ribbon-list').flexslider({
        animation: "slide",
        slideshow: true,
        animationLoop: false,
        itemWidth: 10,
        itemHeight: 10,
        itemMargin: itemMargin,
        minItems: getGridSize(),
        maxItems: getGridSize(),
        pauseOnHover: true,
        controlNav: false,
        mousewheel: false,
        nextText: '', 
        prevText: '',
        start: function(slider){
            $('body').removeClass('loading');
         
            if (slider.find('ul.slides li').length <= getGridSize()) {
                slider.find('.flex-direction-nav').remove();
                var title = slider.parent('.ribbon-list-container').find('.h2-ttl');
                title.addClass('none-link');
                title.find('a').removeAttr("href");
                title.find('.see-all').remove();
            }
            flexslider = slider;

            setSizeMainItem();
        }
    });
}

// Resize flexslider
window.addEventListener('resize', function () {
    setTimeout(function () {
        var slider = $('.ribbon-list').data('flexslider');
        slider.resize();
        removeStyle();
        setSizeMainItem();
    }, 1000);
}, true);

function initFlexSliderArtist() {
    $('.ribbon-list-artist').flexslider({
        animation: "slide",
        slideshow: true,
        animationLoop: false,
        itemWidth: 130,
        itemHeight: 140,
        itemMargin: 45,
        minItems: minItems,
        maxItems: 7,
        pauseOnHover: true,
        controlNav: false,
        mousewheel: false,
        nextText: '', 
        prevText: '',
        start: function(slider){
            $('body').removeClass('loading');
            if (slider.find('ul.slides li').length <= 7) {
                slider.find('.flex-direction-nav').remove();
            }
        },
    });
}

//Listing time for Timeline
function listingTimeLine() {
    if (timeline) {
        timeline.innerHTML = ''; 
        var timelineHTML = '', i;
        for (i = 0; i < 24; i++) {
            timelineHTML = timelineHTML + '<li data-time="' + i + ':00">' + i + ':00</li>';
            timelineHTML = timelineHTML + '<li data-time="' + i + ':15">&nbsp;</li>';
            timelineHTML = timelineHTML + '<li data-time="' + i + ':30">&nbsp;</li>';
            timelineHTML = timelineHTML + '<li data-time="' + i + ':45">&nbsp;</li>';
        }
        timeline.innerHTML = timelineHTML;
    }
}

//Set width each program
function setWidthProgramItem() {
    $.each(programs, function(key, value){
        var secondPlay = $(programs[key]).attr('data-time');
        var widthItem = widthSecond * secondPlay;
        $(programs[key]).css('width', widthItem);
    });
}

/*Schedule timeline-section*/
function getSecondsHours() {
    let d = new Date();
    return d.getHours() * 3600;
}

function getSecondsToDay() {
    let d = new Date();
    return d.getHours() * 3600 + d.getMinutes() * 60 + d.getSeconds();
}

function getPositionOfTime() {
    var second = getSecondsHours();

    var positionTime = widthSecond*second;
    $(timelineContent).css('margin-left', -positionTime );
    $(scheduleChannelContent).css('margin-left', -positionTime );
}

function getPositionOfVerticalLine(){
    var second = getSecondsToDay() - getSecondsHours();
    var positionTime = widthSecond*second;
    $('.vertical-line').css('left', positionTime + 250 );
}

function showMessageAddToWishlist() {
    $.each(addToWishlist, function (key, value) {
        $(addToWishlist[key]).on('click', function(e){
            e.preventDefault();

            var _this = this;
            var channelName = $(_this).attr('data-channel');
            var _html = '';

            if ($(_this).hasClass('fa-heart-o')) {
                $(_this).removeClass('fa-heart-o').addClass('fa-heart');
                $(_this).attr('title','Gỡ kênh yêu thích');
                _html += 'Đã thêm ' + channelName + ' vào danh sách Kênh yêu thich';
            } else {
                $(_this).addClass('fa-heart-o').removeClass('fa-heart');
                $(_this).attr('title','Thêm kênh yêu thích');
                _html += 'Đã gỡ ' + channelName + ' khỏi danh sách Kênh yêu thich';
            }

            $(messagePopup).text(_html);
            $(messagePopup).show();
            setTimeout(function () {
                $(messagePopup).fadeOut('slow');
            }, 3000);
        })
    });
}

function handleControlTimeline() {
    let timeControls = document.querySelectorAll('.schedule-time-control a');
    let second = getSecondsHours();
    let positionTime = widthSecond * second;
    let positionSlide = 3600 * 2 * widthSecond;
    let positionSlideDone = 0;

    $.each(timeControls, function(key, value) {
        if (positionTime < positionSlide/2) {
            if ($(timeControls[key]).hasClass('btn-prev')) {
                $(this).addClass('disabled');
            }
        }

        $(timeControls[key]).on('click', function(e){
            e.preventDefault();
            var _this = $(this);

                if (_this.hasClass('btn-next')) {
                    if ((parseInt(positionTime) + 1) >= maxWidthTimeLine) {
                        _this.addClass('disabled');
                    } else {
                        positionSlideDone = positionTime + positionSlide;
                    }

                    if (_this.prev().hasClass('disabled')) {
                        _this.prev().removeClass('disabled');
                    }
                } else {
                    if (positionTime < positionSlide/2) {
                        if (_this.hasClass('btn-prev')) {
                            _this.addClass('disabled');
                        }

                        positionSlideDone = 0;
                    } else {
                        if (_this.hasClass('btn-prev')) {
                            _this.removeClass('disabled');
                        }

                        if (_this.next().hasClass('disabled')) {
                            _this.next().removeClass('disabled');
                        }
                        positionSlideDone = positionTime - positionSlide;
                    }
                }

            positionTime = positionSlideDone;

            $(timelineContent).css('margin-left', -positionSlideDone );
            $(scheduleChannelContent).css('margin-left', -positionSlideDone );
        })
    });
}

function changeAvatar() {
    let result = document.querySelector('.result'),
    img_result = document.querySelector('.img-result'),
    img_w = document.querySelector('.img-w'),
    img_h = document.querySelector('.img-h'),
    options = document.querySelector('.options'),
    save = document.querySelector('.save'),
    cropped = document.querySelector('.cropped'),
    dwn = document.querySelector('.download'),
    upload = document.querySelector('#file-input'),
    loading = document.querySelector('.loading-avatar'),
    cropper = '';

    if ($(upload).length) {
        // on change show image with crop options
        upload.addEventListener('change', (e) => {
            if (e.target.files.length) {
                // start file reader
                const reader = new FileReader();
                reader.onload = (e)=> {
                    if(e.target.result){
                        loading.classList.add('hide');
                        // create new image
                        let img = document.createElement('img');
                        img.id = 'image';
                        img.src = e.target.result
                        // clean result before
                        result.innerHTML = '';
                        // append new image
                        result.appendChild(img);
                        // show save btn and options
                        save.classList.remove('hide');
                        //options.classList.remove('hide');
                        // init cropper
                        cropper = new Cropper(img, {aspectRatio: 1 / 1});
                    }
                };
                reader.readAsDataURL(e.target.files[0]);
            }
        });

        // save on click
        save.addEventListener('click',(e)=>{
            e.preventDefault();
            // get result to data uri
            let imgSrc = cropper.getCroppedCanvas({
            width: img_w.value // input value
            }).toDataURL();
                // remove hide class of img
                cropped.classList.remove('hide');
                img_result.classList.remove('hide');
                // show image cropped
                cropped.src = imgSrc;
                dwn.classList.remove('hide');
                dwn.download = 'imagename.png';
                dwn.setAttribute('href',imgSrc);
            });
    }
}

function changeNameInput() {
    var inputName = document.querySelector('.input-change-name');
    $(inputName).on('keypress', function (e) {
        var buttonDisabled = $(this).closest('.content-modal').find('.disabled');
        $(buttonDisabled).removeClass('disabled');
    })
}

function setWidthTrailerModal() {
    var mainplayer = document.querySelector('.mainplayer');
    var seeTrialModal = $('#seeTrailerModal');
    var widthMainPlayer = $(mainplayer).innerWidth() + 10;
    var heightMainPlayer = $(mainplayer).innerHeight() + 10;

    seeTrialModal.width(widthMainPlayer).height(heightMainPlayer);
}

function showPlayerControl() {
    $(playerControl).addClass('show-player-control');
    // $('.player-control > i').on('click', function() {
    //     $(this).toggleClass('active'); 
    // })
    
}

function hidePlayerControl() {
    $(playerControl).removeClass('show-player-control');
}

function initControlPlayer() {
    $('.new-back').each(function () {
        var backTxt = $(this).parent().closest('.is-drilldown-submenu-parent').find('> a').clone().children().remove().end().text();
        $(this).html('<i class="material-icons">keyboard_backspace</i> ' + backTxt);
    });
    $(mainPlayer).on('mouseover', function() {
        showPlayerControl();
    }).on('mouseout', function() {
        hidePlayerControl();
    });
}

function initWatchLater() {
    $.each(btnWatchLater, function(key,val) {
        $(btnWatchLater[key]).on('click', function(e) {
            e.preventDefault();
            var _this = this;
            $(_this).toggleClass('selected');
        });
    })
}

function hideOnClickOutside(selector,selectorhide) {
    const outsideClickListener = (event) => {
      if (!$(event.target).closest(selector).length) {
        if ($(selectorhide).is(':visible')) {
          $(selectorhide).hide()
          removeClickListener()
        }
      }
    }
  
    const removeClickListener = () => {
      document.removeEventListener('click', outsideClickListener)
    }
  
    document.addEventListener('click', outsideClickListener)
  }

function initToggleMenu() {
    $('.menu-style-selectbox:not(.menu-select-close) > li > a').on('click', function () {
        var _this = this;
        $('.menu-style-selectbox .is-dropdown-submenu').toggleClass('is-dropdown-submenu-active');
        
        let subMenu = $(_this).parent().find('.submenu li');
        let subMenuLink = $(subMenu).find('a').not('.button');
        let sortButton = $(subMenu).find('.sort');

        $(subMenuLink).on('click', function(e) {
            e.preventDefault();
            $(this).parent('li').find('a').not(this).removeClass('active');
            $(this).addClass('active');
        });

        //Event click sort button
        $(sortButton).on('click', function(e) {
            e.preventDefault();
            $(this).parent().parent('.is-dropdown-submenu').removeClass('is-dropdown-submenu-active');
        });
    });

    $('.menu-select-close > li > a').on('click', function () {
        var _this = this;
        $('.menu-style-selectbox .is-dropdown-submenu').toggleClass('is-dropdown-submenu-active');
        
        let subMenu = $(_this).parent().find('.submenu li');
        let subMenuLink = $(subMenu).find('a').not('.button');

        $(subMenuLink).on('click', function(e) {
            e.preventDefault();
            $(this).parent('li').find('a').not(this).removeClass('active');
            $(this).addClass('active');
            $(this).parent().parent('.is-dropdown-submenu').removeClass('is-dropdown-submenu-active');
        });
    });

    $('body').on('click', function(e) {
        if($(e.target).closest('.menu-style-selectbox').length == 0) {
            $('.menu-style-selectbox .is-dropdown-submenu').removeClass('is-dropdown-submenu-active');
        }
    });
}

function initShowOptionPlayer() {
    $('.boxplayer .player-control > i').on('click', function(e) {
        if(!$(e.target).closest('a').length){
            $(this).find('.options, .volume-option').toggleClass('active');
        }
    })

    $('body').on('click', function(e) {
        if($(e.target).closest('.boxplayer .player-control > i').length == 0) {
            $('.options, .volume-option').removeClass('active');
        }
    });

    $('.boxplayer .options .nested.error li.is-submenu-item a').not('.new-back').on('click', function(e) {
        e.preventDefault();
        $('.options, .volume-option').removeClass('active');
    });

    $('.boxplayer .options .nested li a').not('.new-back').on('click', function(e) {
        e.preventDefault();
        $(this).parent().parent().find('.is-submenu-item').find('a').removeClass('selected');
        $(this).addClass('selected');
    });

    $('.boxplayer .icon-chaplist .options li a').not('.new-back').on('click', function(e) {
        e.preventDefault();
        $(this).parent().find('a').removeClass('selected');
        $(this).addClass('selected');
    });

    $('.boxplayer .icon-chaplist li.is-drilldown-submenu-parent a').not('.new-back').on('click', function(e) {
        e.preventDefault();
        console.log($(this).parent().parent().parent().parent().find('.parent'));
        $(this).parent().parent().parent().parent().parent().find('.parent').removeClass('selected');
        $(this).parent().parent().parent().parent().find('.parent').addClass('selected');
    });
}

function initHeightEmptyState() {
    var heightEmptyState = $(window).height() - ($(header).height() + $(footer).height());
    let contentEmpty = $('.content-empty');
    let artistSection = $('.artist-section');
    
    if ($(artistSection).length) {
        $(contentEmpty).css('height', $(window).height() - 100);
    } else {
        $(contentEmpty).css('height', heightEmptyState);
    }
}

initLoadingDom();

$(document).ready(function () {
    $(document).foundation();
    window.onscroll = function() {
        stickyHeader();
    };
    
    initHeightEmptyState();
    initToggleMenu();
    initOptionMenu();
    initPositionFooter();
    initFlexSlider();
    initFlexSliderArtist();
    showMoreDes();
    showHidePassword();
    listingTimeLine();
    setWidthProgramItem();
    getPositionOfTime();
    getPositionOfVerticalLine();

    setInterval(function(){
        getPositionOfTime();
    },36000);

    setInterval(function(){
        getPositionOfVerticalLine();
    }, 10000);
    
    showMessageAddToWishlist();
    handleControlTimeline();
    changeAvatar();
    changeNameInput();
    setWidthTrailerModal();
    initControlPlayer();
    initWatchLater();
    initShowOptionPlayer();

    setTimeout(() => {
        removeLoadingDom();
    },500)
    
});
