'use strict';

var LikeAction = function() {
        this.item = $('<i class="icon-like-push action-live"></i>');
    },
    HeartAction = function() {
        this.item = $('<i class="icon-heart-push action-live"></i>');
    },
    ActionFactory = function() {
        this.create = function(action) {
            if (action == 'like') {
                return new LikeAction();
            } else {
                return new HeartAction();
            }
        }
    };

var LiveStream = (function() {
    var instance;

    function init() {
        var _aAction = [],
            _stageLike = $('.action-box-like'),
            _stageHeart = $('.action-box-heart'),
            _aFactory = new ActionFactory();

        function _position(action, left, bottom) {
            action.css('left', left);
            action.css('bottom', bottom);
        }

        function create(action, left, bottom) {
            var action = _aFactory.create(action).item;
            _position(action, left, bottom);
            return action;
        } 

        function add(_this ,action) {
            _aAction.push(action);
            $(_this).append(action);

            setTimeout(function() {
                action.remove();
            }, 2500);
        }

        return {
            create:create,
            add: add
        };
    }

    return {
        getInstance: function(){
            if(!instance) {
                instance = init();
            }

            return instance;
        }
    }
})();

$(document).ready(function(){
    $('.action-box').click(function(e){
        var actionData = $(this).attr('data-action');
        var af = LiveStream.getInstance();
        var action = af.create(actionData, Math.floor(Math.random()*30), Math.floor(Math.random()*30));
        af.add(this,action);
    });
});



















